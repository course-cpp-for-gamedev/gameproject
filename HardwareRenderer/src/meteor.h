#pragma once
#include <vector>
#include "physicsobject.h"

class Meteor : public PhysicsObject
{
private:
	std::vector<SDL_Texture*> m_textureList;
	unsigned int m_spriteIndex;
	unsigned int m_animTimeElapsed;
	bool loadAssets(SDL_Renderer* renderer);
	virtual GameObjectType GetObjectType() { return GameObjectType_PlayerShip; }

	virtual bool setPhysicsBody(b2World& world);

	//	PhysicsObject
	virtual void onCollision(PhysicsObject* other);

public:
	Meteor(Position pos);
	bool load(b2World& world, SDL_Renderer* renderer);
	void unload();
	virtual void update(unsigned int);
	virtual void render(SDL_Renderer* renderer);
};