#pragma once
#include <SDL.h>

/*
    This class encapsulates the SDL application - its initialization, its update loop, member variables and uninitialization
*/

class Application
{
public:
    Application();
    bool initialize();
    void run();
    void uninitialize();

private:
    SDL_Window* m_sdlWindow;
    SDL_Renderer* m_renderer;
    bool initializeSDL();
};