#include <iostream>
#include <SDL.h>
#include "game.h"

using namespace std;

World& Game::GetWorld()
{
	return m_world;
}

Game::Game():
	m_isRunning{false},
	m_screenWidth{0},
	m_screenHeight{0}
{
}

void Game::update(unsigned int dt)
{
	//	Update SDL Events
	m_eventManager.update();

	//	Update the world game objects
	m_world.update(dt);

	return;
}

bool Game::initialize(SDL_Renderer* renderer, SDL_Surface* surface)
{
	SDL_DisplayMode displayMode;
	if (SDL_GetRendererOutputSize(renderer, &m_screenWidth, &m_screenHeight))
	{
		cout << "[Game][initialize] Error - " << SDL_GetError();
		return false;
	}

	if (!m_world.load(renderer)) return false;
	m_eventManager.registerEventListener(this);
	m_isRunning = true;

	return true;
}

void Game::render(SDL_Renderer* renderer)
{
	m_world.render(renderer);
}

void Game::onNotificationReceived(const SDL_Event& event)
{
	if (event.type == SDL_QUIT)
		m_isRunning = false;
}

void Game::registerEventListener(IEventListener* listener)
{
	m_eventManager.registerEventListener(listener);
}

void Game::uninitialize()
{
	m_world.uninitialize();
}