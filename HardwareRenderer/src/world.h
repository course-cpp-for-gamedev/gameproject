#pragma once
#include <vector>
#include "box2d.h"
#include "gameobject.h"

class World: public b2ContactListener
{
private:
	b2World m_physicsWorld;
	SDL_Texture* m_backgroundTexture;
	std::vector<GameObject*> m_worldObjects;
	std::vector<GameObject*> m_objectsCreatedLastFrame;
	void AddDeleteWorldObjects();

	bool loadBackground(SDL_Renderer*);

public:
	World();
	bool load(SDL_Renderer*);
	void uninitialize();
	void update(unsigned int dt);
	void render(SDL_Renderer* renderer);

	//	bContactListener
	virtual void BeginContact(b2Contact* contact);

	static void WrapPhysicsBodyPosition(b2Body* physicsBody, b2Vec2 size);
	void createBullet(Position position, b2Vec2 velocity);
};