#include "world.h"
#include <iostream>
#include "game.h"
#include <SDL_image.h>
#include "playership.h"
#include "meteor.h"
#include "bullet.h"

using namespace std;

World::World() :
	m_backgroundTexture(nullptr),
	m_physicsWorld(b2Vec2(0, 0))
{}

bool World::load(SDL_Renderer* renderer)
{
	loadBackground(renderer);
	
	m_physicsWorld.SetContactListener(this);

	//	Create PlayerShip
	PlayerShip* playerShip = new PlayerShip();
	if (!playerShip->load(m_physicsWorld, renderer)) return false;
	m_worldObjects.push_back(playerShip);
	GAME.registerEventListener(playerShip);

	//	Create test Meteor
	Meteor* meteor = new Meteor(Position(200, 200));
	if (!meteor->load(m_physicsWorld, renderer))
		return false;
	m_worldObjects.push_back(meteor);
}

bool World::loadBackground(SDL_Renderer* renderer)
{
	SDL_Surface* backgroundSurface = IMG_Load("data\\BG\\space.png");
	if (!backgroundSurface)
	{
		cout << "[World][loadBackground] IMG_Load Error - " << IMG_GetError();
		return false;
	}

	m_backgroundTexture = SDL_CreateTextureFromSurface(renderer, backgroundSurface);
	if (!backgroundSurface)
	{
		cout << "[World][loadBackground] SDL_CreateTextureFromSurface Error - " << SDL_GetError();
		return false;
	}

	SDL_FreeSurface(backgroundSurface);
	return true;
}

void World::uninitialize()
{
	SDL_DestroyTexture(m_backgroundTexture);
}

void World::update(unsigned int dt)
{
	//	Update Physics
	//	[ToDo] https://gafferongames.com/post/fix_your_timestep/
	m_physicsWorld.Step((float)dt / 1000, 6, 2);

	for (vector<GameObject*>::iterator iter = m_worldObjects.begin(); iter != m_worldObjects.end(); iter++)
		(*iter)->update(dt);

	AddDeleteWorldObjects();
}

void World::render(SDL_Renderer* renderer)
{
	if (SDL_RenderCopy(renderer, m_backgroundTexture, NULL, NULL))
		cout << "[World][Render] SDL_RenderCopy failed for background image";

	//	Render all world objects
	for (vector<GameObject*>::iterator iter = m_worldObjects.begin(); iter != m_worldObjects.end(); iter++)
		(*iter)->render(renderer);
}

void World::BeginContact(b2Contact* contact)
{
	cout << "\n\t[World] WE HAVE CONTACT!!!";
	PhysicsObject* objectA = (PhysicsObject*)contact->GetFixtureA()->GetUserData().pointer;
	PhysicsObject* objectB = (PhysicsObject*)contact->GetFixtureB()->GetUserData().pointer;
	objectA->onCollision(objectB);
	objectB->onCollision(objectA);
}

void World::WrapPhysicsBodyPosition(b2Body* physicsBody, b2Vec2 size)
{
	b2Vec2 currentPosition = physicsBody->GetPosition();

	//	Wrap vertical position
	if (currentPosition.y > GAME.getScreenHeight() + (size.y / 2))
		currentPosition.y = (-1 * (int)size.y / 2);
	else if (currentPosition.y < (-1 * (int)size.y / 2))
		currentPosition.y = GAME.getScreenHeight() + (size.y / 2);

	//	Wrap horizontal position
	if (currentPosition.x > GAME.getScreenWidth() + (size.x / 2))
		currentPosition.x = (-1 * (int)size.x / 2);
	else if (currentPosition.x < (-1 * (int)size.x / 2))
		currentPosition.x = GAME.getScreenWidth() + (size.x / 2);

	physicsBody->SetTransform(currentPosition, physicsBody->GetAngle());
}

void World::createBullet(Position position, b2Vec2 velocity)
{
	Bullet* bullet = new Bullet(position, velocity);
	if (!bullet->load(m_physicsWorld))
		cout << "[World][createBullet] Error in load()\n";
	m_objectsCreatedLastFrame.push_back(bullet);
}

void World::AddDeleteWorldObjects()
{
	//	ToDo: Cleanup deleted objects from last frame
	for (int i = 0; i < m_worldObjects.size(); i++)
		if (m_worldObjects[i]->isDeleted()) {
			m_worldObjects.erase(m_worldObjects.begin() + i);
			i--;
		}

	//	Add new objects from last frame
	while (!m_objectsCreatedLastFrame.empty()) {
		m_worldObjects.push_back(m_objectsCreatedLastFrame.back());
		m_objectsCreatedLastFrame.pop_back();
	}
}
