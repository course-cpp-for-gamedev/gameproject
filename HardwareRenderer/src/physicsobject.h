#pragma once
#include <box2d.h>
#include "gameobject.h"

class PhysicsObject: public GameObject
{
protected:
	b2Body* m_physicsBody;

public:
	PhysicsObject(Position pos) : GameObject(pos), m_physicsBody(nullptr) {};
	virtual bool setPhysicsBody(b2World& world) = 0;
	virtual b2Body* GetPhysicsBody() { return m_physicsBody; }
	virtual void onCollision(PhysicsObject* other) = 0;
};