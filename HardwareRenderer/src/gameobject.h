#pragma once
#include <SDL.h>

class Position
{
public:
	Position(float _x, float _y) : x(_x), y(_y) {}
	float x, y;
};

typedef enum {
	GameObjectType_PlayerShip,
	GameObjectType_Meteor,
	GameObjectType_Bullet
} GameObjectType;

class GameObject
{
protected:
	Position m_position;
	bool m_loaded;
	bool m_deleted;

public:
	GameObject(Position pos) : m_position(pos), m_loaded(false), m_deleted(false) {}
	virtual void update(unsigned int dt) = 0;
	virtual void render(SDL_Renderer*) = 0;
	virtual GameObjectType GetObjectType() = 0;
	bool isDeleted() { return m_deleted; }
};