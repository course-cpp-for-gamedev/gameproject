#pragma once
#include <SDL.h>
#include "world.h"
#include "eventmanager.h"

class Game: public IEventListener
{
public:
	//	https://gameprogrammingpatterns.com/singleton.html
	static Game& getInstance()
	{
		static Game* instance = new Game();
		return *instance;
	}

	bool isRunning() { return m_isRunning; }
	bool initialize(SDL_Renderer*, SDL_Surface*);
	void uninitialize();
	void update(unsigned int);
	void render(SDL_Renderer* renderer);

	virtual void onNotificationReceived(const SDL_Event& event);
	void registerEventListener(IEventListener*);

	int getScreenWidth() { return m_screenWidth; }
	int getScreenHeight() { return m_screenHeight; }
	World& GetWorld();

private:
	Game();
	bool m_isRunning;

	//	Event Manager
	EventManager m_eventManager;

	//	Game holds the World
	World m_world;

	int m_screenWidth;
	int m_screenHeight;
};

#define GAME Game::getInstance()
