#pragma once
#include "physicsobject.h"

class Bullet : public PhysicsObject
{
private:
	SDL_Surface* m_surface;
	virtual bool setPhysicsBody(b2World& world);
	bool loadAssets();
	virtual GameObjectType GetObjectType() { return GameObjectType_Bullet; }

	//	PhysicsObject
	virtual void onCollision(PhysicsObject* other);

	b2Vec2 m_spawnVelocity;

public:
	Bullet(Position pos, b2Vec2 spawnVelocity) :PhysicsObject(pos), m_spawnVelocity(spawnVelocity), m_surface(NULL) {}
	virtual void update(unsigned int);
	virtual void render(SDL_Renderer*);
	bool load(b2World&);
	static int GetRadius();
};
