#pragma once
#include "physicsobject.h"
#include "eventmanager.h"

class PlayerShip : public PhysicsObject, public IEventListener
{
private:
	std::vector<SDL_Texture*> m_textureList;
	unsigned int m_spriteIndex;
	unsigned int m_animTimeElapsed;
	bool loadAssets(SDL_Renderer*);

	bool m_bForwardPressed;
	bool m_bBackPressed;
	bool m_bLeftPressed;
	bool m_bRightPressed;
	bool m_bShootPressed;

	unsigned int m_timeSinceLastBullet;
	void shootBullet();

	virtual bool setPhysicsBody(b2World& world);
	void applyForce_Forward(unsigned int dt);
	void applyForce_Backward(unsigned int dt);
	void applyForce_Clockwise(unsigned int dt);
	void applyForce_Anticlockwise(unsigned int dt);

	SDL_Rect m_shipImage;

public:
	PlayerShip();
	virtual void update(unsigned int dt);
	virtual void render(SDL_Renderer*);
	bool load(b2World&, SDL_Renderer*);
	void unload();
	virtual GameObjectType GetObjectType() { return GameObjectType_PlayerShip; }

	//	EventListener
	virtual void onNotificationReceived(const SDL_Event& event);

	//	PhysicsObject
	virtual void onCollision(PhysicsObject* other);
};