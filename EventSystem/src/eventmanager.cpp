#include "eventmanager.h"

void EventManager::registerEventListener(IEventListener* listener)
{
	if (listener != nullptr)
		m_eventListeners.push_back(listener);
}

void EventManager::update()
{
	//	Check for input events
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		for (std::vector<IEventListener*>::iterator iter = m_eventListeners.begin(); iter != m_eventListeners.end(); iter++)
			(*iter)->onNotificationReceived(event);
	}
}
