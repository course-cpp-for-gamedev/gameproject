#include <iostream>
#include <SDL.h>
#include "game.h"

using namespace std;

const int IMAGE_BORDER_SIZE = 40;

Game::Game(): m_isRunning{false}, m_sdlSurface(nullptr), m_screenWidth {0}, m_screenHeight{0}
{
}

void Game::update()
{
	//	Update SDL Events
	m_eventManager.update();

	return;
}

bool Game::initialize(SDL_Surface* surface)
{
	m_sdlSurface = surface;
	m_screenWidth = surface->w;
	m_screenHeight = surface->h;
	m_isRunning = true;

	m_eventManager.registerEventListener(this);

	return true;
}

void Game::render()
{
	SDL_Rect innerRect; innerRect.x = IMAGE_BORDER_SIZE; innerRect.y = IMAGE_BORDER_SIZE; innerRect.w = m_screenWidth - 2 * IMAGE_BORDER_SIZE; innerRect.h = m_screenHeight - 2 * IMAGE_BORDER_SIZE;
	SDL_FillRect(m_sdlSurface, nullptr, SDL_MapRGB(m_sdlSurface->format, 0x00, 0x00, 0x00));
	SDL_FillRect(m_sdlSurface, &innerRect, SDL_MapRGB(m_sdlSurface->format, 0xff, 0xff, 0xff));
}

void Game::onNotificationReceived(const SDL_Event& event)
{
	if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
		cout << endl << "Event Received!: " << "scancode - " << event.key.keysym.scancode << ", type - " << event.key.type << endl;

	if (event.type == SDL_QUIT)
		m_isRunning = false;
}