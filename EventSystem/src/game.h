#pragma once
#include <SDL.h>
#include "eventmanager.h"

class Game: public IEventListener
{
public:
	//	https://gameprogrammingpatterns.com/singleton.html
	static Game& getInstance()
	{
		static Game* instance = new Game();
		return *instance;
	}

	bool isRunning() { return m_isRunning; }
	bool initialize(SDL_Surface*);
	void update();
	void render();

	virtual void onNotificationReceived(const SDL_Event& event);

private:
	Game();
	bool m_isRunning;

	//	Event Manager
	EventManager m_eventManager;

	//	SDL Window/Surface management
	SDL_Surface* m_sdlSurface;
	int m_screenWidth;
	int m_screenHeight;
};

#define GAME Game::getInstance()
