#pragma once
#include <SDL.h>
#include <vector>

class IEventListener
{
public:
    virtual void onNotificationReceived(const SDL_Event& event) = 0;
};

class EventManager
{
private:
    std::vector<IEventListener*> m_eventListeners;

public:
    EventManager() {};
    void registerEventListener(IEventListener*);
    void update();
};