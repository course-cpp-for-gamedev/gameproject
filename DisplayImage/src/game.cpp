#include "game.h"
#include <SDL.h>

const int IMAGE_BORDER_SIZE = 40;

Game::Game(): m_isRunning{false}, m_sdlSurface(nullptr), m_screenWidth {0}, m_screenHeight{0}
{
}

void Game::update()
{
	//	Check for input events
	SDL_Event event;
	while (SDL_PollEvent(&event))
		if (event.type == SDL_QUIT) m_isRunning = false;

	return;
}

bool Game::initialize(SDL_Surface* surface)
{
	m_sdlSurface = surface;
	m_screenWidth = surface->w;
	m_screenHeight = surface->h;
	m_isRunning = true;
	return true;
}

void Game::uninitialize() {}

void Game::render()
{
	SDL_Rect innerRect; innerRect.x = IMAGE_BORDER_SIZE; innerRect.y = IMAGE_BORDER_SIZE; innerRect.w = m_screenWidth - 2 * IMAGE_BORDER_SIZE; innerRect.h = m_screenHeight - 2 * IMAGE_BORDER_SIZE;
	SDL_FillRect(m_sdlSurface, nullptr, SDL_MapRGB(m_sdlSurface->format, 0x00, 0x00, 0x00));
	SDL_FillRect(m_sdlSurface, &innerRect, SDL_MapRGB(m_sdlSurface->format, 0xff, 0xff, 0xff));
}