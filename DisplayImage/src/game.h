#pragma once
#include <SDL.h>

class Game
{
public:
	//	https://gameprogrammingpatterns.com/singleton.html
	static Game& getInstance()
	{
		static Game* instance = new Game();
		return *instance;
	}

	bool isRunning() { return m_isRunning; }
	bool initialize(SDL_Surface*);
	void uninitialize();
	void update();
	void render();

private:
	Game();
	bool m_isRunning;
	SDL_Surface* m_sdlSurface;
	int m_screenWidth;
	int m_screenHeight;
};

#define GAME Game::getInstance()
