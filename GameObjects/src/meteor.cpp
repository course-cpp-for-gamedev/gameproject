#include <SDL_image.h>
#include <string>
#include <iostream>
#include "meteor.h"

using namespace std;

const string ASSET_PATH = "data\\Asteroid\\Asteroid 01-.";
const string ASSET_EXTENSION = ".png";
const unsigned int IMAGE_COUNT = 60;
const unsigned int ANIMATION_DELAY = 16; // in ms

bool Meteor::load()
{
    for (int i = 1; i <= IMAGE_COUNT; i++)
    {
        SDL_Surface* surface = IMG_Load((ASSET_PATH + to_string(i) + ASSET_EXTENSION).c_str());
        
        if (surface == NULL) {
            cout << "\n[Meteor][load] Error: IMG_Load failed - " << IMG_GetError();
            return false;
        }

        m_surfaceList.push_back(surface);
    }

    m_spriteIndex = 0;
    m_loaded = true;
    return true;
}

void Meteor::unload()
{
    if (m_loaded)
    {
        for (auto surface : m_surfaceList)
        {
            SDL_FreeSurface(surface);
        }
    }
}

void Meteor::update(unsigned int dt)
{
    if (!m_loaded) return;

    //  Update sprite animation
    m_animTimeElapsed += dt;
    m_spriteIndex += m_animTimeElapsed / ANIMATION_DELAY;
    m_spriteIndex = m_spriteIndex % IMAGE_COUNT;
    m_animTimeElapsed = m_animTimeElapsed % ANIMATION_DELAY;
}

void Meteor::render(SDL_Surface* dstSurface)
{
    if (!m_loaded) return;

    SDL_Rect dstRect;
    SDL_GetClipRect(m_surfaceList[m_spriteIndex], &dstRect);
    dstRect.x = m_position.x;
    dstRect.y = m_position.y;

    if (SDL_BlitSurface(m_surfaceList[m_spriteIndex], nullptr, dstSurface, &dstRect))
        cout << "\n[Meteor][render] Error: SDL_BlitSurface failed for background image - " << SDL_GetError();
}