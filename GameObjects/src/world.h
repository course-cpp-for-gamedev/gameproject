#pragma once
#include <vector>
#include "gameobject.h"

class World
{
private:
	SDL_Surface* m_backgroundSurface;
	std::vector<GameObject*> m_worldObjects;

public:
	World();
	void initialize();
	void uninitialize();
	void update(unsigned int dt);
	void render(SDL_Surface*);
};