#pragma once
#include <SDL.h>
#include "world.h"
#include "eventmanager.h"

class Game: public IEventListener
{
public:
	//	https://gameprogrammingpatterns.com/singleton.html
	static Game& getInstance()
	{
		static Game* instance = new Game();
		return *instance;
	}

	bool isRunning() { return m_isRunning; }
	bool initialize(SDL_Surface*);
	void uninitialize();
	void update(unsigned int);
	void render();

	virtual void onNotificationReceived(const SDL_Event& event);
	void registerEventListener(IEventListener*);

private:
	Game();
	bool m_isRunning;

	//	Event Manager
	EventManager m_eventManager;

	//	Game holds the World
	World m_world;

	//	SDL Window/Surface management
	SDL_Surface* m_sdlSurface;
	int m_screenWidth;
	int m_screenHeight;
};

#define GAME Game::getInstance()
