#pragma once
#include "gameobject.h"
#include "eventmanager.h"

class PlayerShip: public GameObject, public IEventListener
{
private:
	bool m_bForwardPressed;
	bool m_bBackPressed;
	bool m_bLeftPressed;
	bool m_bRightPressed;
	bool m_bShootPressed;

	SDL_Rect m_shipImage;

public:
	PlayerShip();
	virtual void update(unsigned int dt);
	virtual void render(SDL_Surface*);

//	EventListener
	virtual void onNotificationReceived(const SDL_Event& event);
};