#pragma once
#include "gameobject.h"

class Bullet : public GameObject
{
private:

public:
	virtual void update();
	virtual void render(SDL_Surface*);
};
