#pragma once
#include <SDL.h>

class Position
{
public:
	Position(float _x, float _y) : x(_x), y(_y) {}
	float x, y;
};

class GameObject
{
protected:
	Position m_position;
	bool m_loaded;

public:
	GameObject(Position pos) : m_position(pos), m_loaded(false) {}
	virtual void update(unsigned int dt) = 0;
	virtual void render(SDL_Surface*) = 0;
};