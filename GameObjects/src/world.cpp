#include "world.h"
#include <iostream>
#include "game.h"
#include <SDL_image.h>
#include "playership.h"
#include "meteor.h"

using namespace std;

World::World(): m_backgroundSurface(nullptr)
{}

void World::initialize()
{
	//	Load Background
	m_backgroundSurface = IMG_Load("data\\BG\\space.png");
	if (m_backgroundSurface == nullptr)
		cout << "IMG_Load Error: " << IMG_GetError();

	//	Create PlayerShip
	PlayerShip* playerShip = new PlayerShip();
	m_worldObjects.push_back(playerShip);
	GAME.registerEventListener(playerShip);

	//	Create test Meteor
	Meteor* meteor = new Meteor(Position(200, 200));
	if (meteor->load())
		m_worldObjects.push_back(meteor);
}

void World::uninitialize()
{
	SDL_FreeSurface(m_backgroundSurface);
}

void World::update(unsigned int dt)
{
	for (vector<GameObject*>::iterator iter = m_worldObjects.begin(); iter != m_worldObjects.end(); iter++)
		(*iter)->update(dt);
}

void World::render(SDL_Surface* dstSurface)
{
	//	Draw the background image 
	SDL_Rect dstRect;
	SDL_GetClipRect(dstSurface, &dstRect);

	if (SDL_BlitSurface(m_backgroundSurface, nullptr, dstSurface, &dstRect))
		cout << "[World][Render] SDL_BlitSurface failed for background image";

	//	Render all world objects
	for (vector<GameObject*>::iterator iter = m_worldObjects.begin(); iter != m_worldObjects.end(); iter++)
		(*iter)->render(dstSurface);
}