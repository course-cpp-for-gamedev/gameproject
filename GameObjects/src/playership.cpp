#include "playership.h"

const float SHIP_VELOCITY = 0.7f;

PlayerShip::PlayerShip():
	GameObject(Position(512, 512)),
	m_bBackPressed(false),
	m_bForwardPressed(false),
	m_bLeftPressed(false),
	m_bRightPressed(false),
	m_bShootPressed(false)
{
	m_shipImage.w = m_shipImage.h = 24;
}

void PlayerShip::update(unsigned int dt)
{
	if (m_bForwardPressed) m_position.y -= SHIP_VELOCITY * dt;
	if (m_bBackPressed) m_position.y += SHIP_VELOCITY * dt;
	if (m_bLeftPressed) m_position.x -= SHIP_VELOCITY * dt;
	if (m_bRightPressed) m_position.x += SHIP_VELOCITY * dt;

	m_shipImage.x = m_position.x;
	m_shipImage.y = m_position.y;
}

void PlayerShip::render(SDL_Surface* dstSurface)
{
	SDL_FillRect(dstSurface, &m_shipImage, SDL_MapRGB(dstSurface->format, 0xff, 0xff, 0xff));
}

void PlayerShip::onNotificationReceived(const SDL_Event& event)
{
	switch (event.type)
	{
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym)
		{
		case 'a':
		case SDLK_LEFT:
			m_bLeftPressed = true;
			break;
		case 'd':
		case SDLK_RIGHT:
			m_bRightPressed = true;
			break;
		case 'w':
		case SDLK_UP:
			m_bForwardPressed = true;
			break;
		case 's':
		case SDLK_DOWN:
			m_bBackPressed = true;
			break;
		case SDLK_SPACE:
			m_bShootPressed = true;
			break;
		}
		break;

	case SDL_KEYUP:
		switch (event.key.keysym.sym)
		{
		case 'a':
		case SDLK_LEFT:
			m_bLeftPressed = false;
			break;
		case 'd':
		case SDLK_RIGHT:
			m_bRightPressed = false;
			break;
		case 'w':
		case SDLK_UP:
			m_bForwardPressed = false;
			break;
		case 's':
		case SDLK_DOWN:
			m_bBackPressed = false;
			break;
		case SDLK_SPACE:
			m_bShootPressed = false;
			break;
		}
		break;
	}
}