
#include "application.h"

int main(int argc, char* argv[]) {

	Application sdlApp;
	if (!sdlApp.initialize())
		return 1;
	sdlApp.run();
	sdlApp.uninitialize();

	return 0;
}
