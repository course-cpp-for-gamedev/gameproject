#pragma once
#include "gameobject.h"
#include <vector>

class Meteor : public GameObject
{
private:
	std::vector<SDL_Surface*> m_surfaceList;
	unsigned int m_spriteIndex;
	unsigned int m_animTimeElapsed;

public:
	Meteor(Position pos): GameObject(pos) {}
	bool load();
	void unload();
	virtual void update(unsigned int);
	virtual void render(SDL_Surface*);
};