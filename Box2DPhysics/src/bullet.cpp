#include <SDL_image.h>
#include <string>
#include <iostream>
#include "bullet.h"
#include "game.h"

const std::string IMAGE_PATH = "data\\PlayerShip\\pulse.bmp";
const unsigned int IMAGE_WIDTH = 16;
const unsigned int IMAGE_HEIGHT = 14;

int Bullet::GetRadius()
{
    return IMAGE_WIDTH / 2;
}

bool Bullet::setPhysicsBody(b2World& world)
{
    if (m_physicsBody)
        world.DestroyBody(m_physicsBody);

    //	Setup physics object
    b2BodyDef bodyDef;
    bodyDef.type = b2_kinematicBody;
    bodyDef.position.Set(m_position.x + IMAGE_WIDTH / 2, m_position.y + IMAGE_HEIGHT / 2);
    m_physicsBody = world.CreateBody(&bodyDef);
    if (m_physicsBody == nullptr) return false;

    b2CircleShape circleShape;
    circleShape.m_radius = GetRadius();
    b2FixtureDef fixtureDef;
    fixtureDef.userData.pointer = (uintptr_t)this;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 2.0f;
    fixtureDef.friction = 0.3f;
    fixtureDef.restitution = 0.7f;
    if (!m_physicsBody->CreateFixture(&fixtureDef)) return false;

    m_physicsBody->SetLinearVelocity(m_spawnVelocity);
    return true;
}

bool Bullet::loadAssets()
{
    m_surface = IMG_Load((IMAGE_PATH).c_str());
    if (m_surface == NULL) {
        std::cout << "\n[Meteor][load] Error: IMG_Load failed - " << IMG_GetError();
        return false;
    }
    return true;
}

void Bullet::onCollision(PhysicsObject* other)
{
    if (other->GetObjectType() == GameObjectType_Meteor)
        std::cout << "We hit !";
}

void Bullet::update(unsigned int dt)
{
    b2Vec2 physicsPosition = m_physicsBody->GetPosition();
    m_position.x = physicsPosition.x - IMAGE_WIDTH / 2;
    m_position.y = physicsPosition.y - IMAGE_HEIGHT / 2;

    //  Mark for delete if we are out of the view frame
    if (m_position.x < 0 - (int)IMAGE_WIDTH ||
        m_position.x > GAME.getScreenWidth() ||
        m_position.y < 0 - (int)IMAGE_HEIGHT ||
        m_position.y > GAME.getScreenHeight())
        m_deleted = true;
}

void Bullet::render(SDL_Surface* dstSurface)
{
    SDL_Rect dstRect{
        (int)m_position.x,
        (int)m_position.y,
        IMAGE_WIDTH,
        IMAGE_HEIGHT
    };

    if (SDL_BlitSurface(m_surface, nullptr, dstSurface, &dstRect))
        std::cout << "\n[Meteor][render] Error: SDL_BlitSurface failed for background image - " << SDL_GetError();
}

bool Bullet::load(b2World& world)
{
    return loadAssets() && setPhysicsBody(world);
}
