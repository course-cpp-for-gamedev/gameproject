#include <SDL_image.h>
#include "application.h"
#include "game.h"
#include <iostream>
using namespace std;

const int WINDOW_HEIGHT = 1024, WINDOW_WIDTH = 1024;

Application::Application() : m_sdlWindow(nullptr)
{
}

bool Application::initialize()
{
	if (!initializeSDL())
		return false;

	return GAME.initialize(SDL_GetWindowSurface(m_sdlWindow));
}

void Application::run()
{
	unsigned int lastUpdateTime = SDL_GetTicks(), dt = 0;
	while (GAME.isRunning())
	{
		//	Calculate frame dt
		dt = SDL_GetTicks() - lastUpdateTime;
		lastUpdateTime = SDL_GetTicks();

		//	Update game state
		GAME.update(dt);		
			
		//	Render game elements
		GAME.render();

		// Flip the backbuffer
		SDL_UpdateWindowSurface(m_sdlWindow);
	}
}

void Application::uninitialize()
{
	GAME.uninitialize();

	//Destroy window
	SDL_DestroyWindow(m_sdlWindow);

	//Quit SDL subsystems
	SDL_Quit();
}

bool Application::initializeSDL()
{
	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
		return false;
	}

	if (!IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG))
	{
		cout << "IMG_Init could not initialize! IMG_Error: " << IMG_GetError() << endl;
		return false;
	}

	// Create Window
	m_sdlWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	if (m_sdlWindow == NULL)
	{
		cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
		return false;
	}

	return true;
}