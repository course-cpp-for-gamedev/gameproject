#include <iostream>
#include <SDL.h>
#include "game.h"

using namespace std;

World& Game::GetWorld()
{
	return m_world;
}

Game::Game():
	m_isRunning{false},
	m_sdlSurface(nullptr),
	m_screenWidth {0},
	m_screenHeight{0}
{
}

void Game::update(unsigned int dt)
{
	//	Update SDL Events
	m_eventManager.update();

	//	Update the world game objects
	m_world.update(dt);

	return;
}

bool Game::initialize(SDL_Surface* surface)
{
	m_sdlSurface = surface;
	m_screenWidth = surface->w;
	m_screenHeight = surface->h;
	m_isRunning = true;

	if (!m_world.initialize()) return false;

	m_eventManager.registerEventListener(this);

	return true;
}

void Game::render()
{
	m_world.render(m_sdlSurface);
}

void Game::onNotificationReceived(const SDL_Event& event)
{
	if (event.type == SDL_QUIT)
		m_isRunning = false;
}

void Game::registerEventListener(IEventListener* listener)
{
	m_eventManager.registerEventListener(listener);
}

void Game::uninitialize()
{
	m_world.uninitialize();
}