#pragma once
#include <vector>
#include "physicsobject.h"

class Meteor : public PhysicsObject
{
private:
	std::vector<SDL_Surface*> m_surfaceList;
	unsigned int m_spriteIndex;
	unsigned int m_animTimeElapsed;
	bool loadAssets();
	virtual GameObjectType GetObjectType() { return GameObjectType_PlayerShip; }

	virtual bool setPhysicsBody(b2World& world);

	//	PhysicsObject
	virtual void onCollision(PhysicsObject* other);

public:
	Meteor(Position pos);
	bool load(b2World& world);
	void unload();
	virtual void update(unsigned int);
	virtual void render(SDL_Surface*);
};