#include <SDL_image.h>
#include <string>
#include <iostream>
#include "world.h"
#include "meteor.h"

using namespace std;

const string IMAGE_PATH = "data\\Asteroid\\Asteroid 01-.";
const string IMAGE_EXTENSION = ".png";
const unsigned int IMAGE_COUNT = 60;
const unsigned int IMAGE_WIDTH = 128;
const unsigned int IMAGE_HEIGHT = 128;
const unsigned int ANIMATION_DELAY = 16; // in ms

Meteor::Meteor(Position pos):
    PhysicsObject(pos),
    m_animTimeElapsed(0),
    m_spriteIndex(0)
{}

bool Meteor::load(b2World& world)
{
    m_loaded = setPhysicsBody(world) && loadAssets();

    //  Test
    m_physicsBody->ApplyLinearImpulseToCenter(b2Vec2(2000000, 2000000), true);

    return m_loaded;
}

void Meteor::unload()
{
    if (m_loaded)
    {
        for (auto surface : m_surfaceList)
        {
            SDL_FreeSurface(surface);
        }
    }
    m_loaded = false;
}

void Meteor::update(unsigned int dt)
{
    if (!m_loaded) return;

    //  Update sprite animation
    m_animTimeElapsed += dt;
    m_spriteIndex += m_animTimeElapsed / ANIMATION_DELAY;
    m_spriteIndex = m_spriteIndex % IMAGE_COUNT;
    m_animTimeElapsed = m_animTimeElapsed % ANIMATION_DELAY;

    World::WrapPhysicsBodyPosition(m_physicsBody, b2Vec2(IMAGE_WIDTH, IMAGE_HEIGHT));
    b2Vec2 physicsPosition = m_physicsBody->GetPosition();
    m_position.x = physicsPosition.x - IMAGE_WIDTH / 2;
    m_position.y = physicsPosition.y - IMAGE_HEIGHT / 2;
}

void Meteor::onCollision(PhysicsObject* other)
{
    if (other->GetObjectType() == GameObjectType_PlayerShip)
        cout << "\n\t [Meteor]: WE HAVE COLLISION! ...with a PlayerShip!";
}

void Meteor::render(SDL_Surface* dstSurface)
{
    if (!m_loaded) return;

    SDL_Rect dstRect{
        (int)m_position.x,
        (int)m_position.y,
        IMAGE_WIDTH,
        IMAGE_HEIGHT
    };

    if (SDL_BlitSurface(m_surfaceList[m_spriteIndex], nullptr, dstSurface, &dstRect))
        cout << "\n[Meteor][render] Error: SDL_BlitSurface failed for background image - " << SDL_GetError();
}

bool Meteor::setPhysicsBody(b2World& world)
{
    if (m_physicsBody)
        world.DestroyBody(m_physicsBody);

    //	Setup physics object
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(m_position.x + IMAGE_WIDTH / 2, m_position.y + IMAGE_HEIGHT / 2);
    m_physicsBody = world.CreateBody(&bodyDef);
    if (m_physicsBody == nullptr) return false;

    b2CircleShape circleShape;
    circleShape.m_radius = IMAGE_WIDTH / 2;
    b2FixtureDef fixtureDef;
    fixtureDef.userData.pointer = (uintptr_t)this;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 2.0f;
    fixtureDef.friction = 0.3f;
    fixtureDef.restitution = 0.7f;
    if (!m_physicsBody->CreateFixture(&fixtureDef)) return false;

    return true;
}

bool Meteor::loadAssets()
{
    for (int i = 1; i <= IMAGE_COUNT; i++)
    {
        SDL_Surface* surface = IMG_Load((IMAGE_PATH + to_string(i) + IMAGE_EXTENSION).c_str());
        if (surface == NULL) {
            cout << "\n[Meteor][load] Error: IMG_Load failed - " << IMG_GetError();
            return false;
        }
        m_surfaceList.push_back(surface);
    }

    m_spriteIndex = 0;
    return true;
}