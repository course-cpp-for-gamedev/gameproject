#include <string>
#include <iostream>
#include <SDL_image.h>
#include "world.h"
#include "playership.h"
#include "game.h"
#include "bullet.h"

using namespace std;

const string IMAGE_PATH = "data\\PlayerShip\\PlayerShip_0_";
const string IMAGE_EXTENSION = ".bmp";
const unsigned int IMAGE_COUNT = 8;
const unsigned int ANIMATION_DELAY = 125;
const unsigned int IMAGE_WIDTH = 128;
const unsigned int IMAGE_HEIGHT = 128;

const int SHIP_ACCELERATION = 100;
const float SHIP_VELOCITY_MAX = 100.0f;
const float SHIP_ANGULAR_ACCELERATION = 180.0f * ((float)M_PI / 180);	// in radians
const float SHIP_ANGULAR_VELOCITY_MAX = 30.0f * ((float)M_PI / 180);
const float VELOCITY_MAX = 5.0f;
const unsigned int BULLET_SPEED = 300;
const unsigned int BULLET_DELAY = 200;

PlayerShip::PlayerShip() :
	PhysicsObject(Position(512 - IMAGE_WIDTH/2, 512 - IMAGE_HEIGHT/2)),
	m_bBackPressed(false),
	m_bForwardPressed(false),
	m_bLeftPressed(false),
	m_bRightPressed(false),
	m_bShootPressed(false),
	m_timeSinceLastBullet(BULLET_DELAY),
	m_animTimeElapsed(0),
	m_spriteIndex(0)
{
	m_shipImage.w = m_shipImage.h = 24;
}

bool PlayerShip::load(b2World& world)
{
	m_loaded = setPhysicsBody(world) && loadAssets();
	return m_loaded;
}
void PlayerShip::unload()
{
	if (m_loaded)
	{
		for (auto surface : m_surfaceList)
		{
			SDL_FreeSurface(surface);
		}
		m_loaded = false;
	}
}

void PlayerShip::update(unsigned int dt)
{
	m_timeSinceLastBullet += dt;
	//	Update sprite animation
	m_animTimeElapsed += dt;
	m_spriteIndex += m_animTimeElapsed / ANIMATION_DELAY;
	m_spriteIndex = m_spriteIndex % IMAGE_COUNT;
	m_animTimeElapsed = m_animTimeElapsed % ANIMATION_DELAY;

	//	Process Input
	if (m_bForwardPressed) applyForce_Forward(dt);
	if (m_bBackPressed) applyForce_Backward(dt);
	if (m_bLeftPressed) applyForce_Anticlockwise(dt);
	if (m_bRightPressed) applyForce_Clockwise(dt);
	if (m_bShootPressed && m_timeSinceLastBullet > BULLET_DELAY) shootBullet();
	
	//	 Clamp linear velocity
	b2Vec2 velocity = m_physicsBody->GetLinearVelocity();
	float speed = velocity.Normalize();
	velocity *= min(SHIP_VELOCITY_MAX, speed);
	m_physicsBody->SetLinearVelocity(velocity);
	//	Clamp angular velocity
	float angularVelocity = m_physicsBody->GetAngularVelocity();
	angularVelocity = min(angularVelocity, SHIP_ANGULAR_VELOCITY_MAX);
	m_physicsBody->SetAngularVelocity(angularVelocity);

	World::WrapPhysicsBodyPosition(m_physicsBody, b2Vec2(IMAGE_WIDTH, IMAGE_HEIGHT));
	b2Vec2 physicsPosition = m_physicsBody->GetPosition();
	m_position.x = physicsPosition.x - IMAGE_WIDTH/2;
	m_position.y = physicsPosition.y - IMAGE_HEIGHT/2;
}

void PlayerShip::applyForce_Forward(unsigned int dt)
{
	b2Vec2 forwardVector = m_physicsBody->GetWorldVector(b2Vec2(0, -1));
	forwardVector *= SHIP_ACCELERATION;
	m_physicsBody->ApplyForceToCenter(m_physicsBody->GetMass() * dt * forwardVector, true);
}

void PlayerShip::applyForce_Backward(unsigned int dt)
{
	b2Vec2 forwardVector = m_physicsBody->GetWorldVector(b2Vec2(0, 1));
	forwardVector *= SHIP_ACCELERATION;
	m_physicsBody->ApplyForceToCenter(m_physicsBody->GetMass() * dt * forwardVector, true);
}

void PlayerShip::applyForce_Clockwise(unsigned int dt)
{
	m_physicsBody->ApplyTorque(m_physicsBody->GetInertia() * SHIP_ANGULAR_ACCELERATION, true);
}

void PlayerShip::applyForce_Anticlockwise(unsigned int dt)
{
	m_physicsBody->ApplyTorque(-1 * m_physicsBody->GetInertia() * SHIP_ANGULAR_ACCELERATION, true);
}

void PlayerShip::shootBullet()
{
	b2Vec2 forwardVector = m_physicsBody->GetWorldVector(b2Vec2(0, -1));
	b2Vec2 bulletVelocity = BULLET_SPEED * forwardVector + m_physicsBody->GetLinearVelocity();
	b2Vec2 bulletPhysicsPosition = (IMAGE_WIDTH / 2 + Bullet::GetRadius()) * forwardVector + m_physicsBody->GetPosition();
	Position worldPosition = Position(bulletPhysicsPosition.x - Bullet::GetRadius(), bulletPhysicsPosition.y - Bullet::GetRadius());
	GAME.GetWorld().createBullet(worldPosition, bulletVelocity);
	m_timeSinceLastBullet = 0;
}

void PlayerShip::render(SDL_Surface* dstSurface)
{
	if (!m_loaded) return;

	SDL_Rect dstRect {
		(int)m_position.x,
		(int)m_position.y,
		IMAGE_WIDTH,
		IMAGE_HEIGHT
	};

	if (SDL_BlitSurface(m_surfaceList[m_spriteIndex], nullptr, dstSurface, &dstRect))
		cout << "\n[Meteor][render] Error: SDL_BlitSurface failed for background image - " << SDL_GetError();
}

void PlayerShip::onNotificationReceived(const SDL_Event& event)
{
	switch (event.type)
	{
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym)
		{
		case 'a':
		case SDLK_LEFT:
			m_bLeftPressed = true;
			break;
		case 'd':
		case SDLK_RIGHT:
			m_bRightPressed = true;
			break;
		case 'w':
		case SDLK_UP:
			m_bForwardPressed = true;
			break;
		case 's':
		case SDLK_DOWN:
			m_bBackPressed = true;
			break;
		case SDLK_SPACE:
			m_bShootPressed = true;
			break;
		}
		break;

	case SDL_KEYUP:
		switch (event.key.keysym.sym)
		{
		case 'a':
		case SDLK_LEFT:
			m_bLeftPressed = false;
			break;
		case 'd':
		case SDLK_RIGHT:
			m_bRightPressed = false;
			break;
		case 'w':
		case SDLK_UP:
			m_bForwardPressed = false;
			break;
		case 's':
		case SDLK_DOWN:
			m_bBackPressed = false;
			break;
		case SDLK_SPACE:
			m_bShootPressed = false;
			break;
		}
		break;
	}
}

bool PlayerShip::setPhysicsBody(b2World& world)
{
	if (m_physicsBody)
		world.DestroyBody(m_physicsBody);

	//	Setup physics object
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(m_position.x + IMAGE_WIDTH / 2, m_position.y + IMAGE_HEIGHT / 2);
	m_physicsBody = world.CreateBody(&bodyDef);
	if (m_physicsBody == nullptr) return false;

	b2CircleShape circleShape;
	circleShape.m_radius = IMAGE_WIDTH / 2;
	b2FixtureDef fixtureDef;
	fixtureDef.userData.pointer = (uintptr_t)this;
	fixtureDef.shape = &circleShape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.7f;
	if (!m_physicsBody->CreateFixture(&fixtureDef)) return false;

	return true;
}

void PlayerShip::onCollision(PhysicsObject* other)
{
	cout << "\n\t [PlayerShip] WE HAVE COLLISION!";
}

bool PlayerShip::loadAssets()
{
	for (int i = 0; i < IMAGE_COUNT; i++)
	{
		SDL_Surface* surface = IMG_Load((IMAGE_PATH + to_string(i) + IMAGE_EXTENSION).c_str());

		if (surface == NULL) {
			cout << "\n[Meteor][load] Error: IMG_Load failed - " << IMG_GetError();
			return false;
		}

		SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0, 0));
		m_surfaceList.push_back(surface);
	}

	m_spriteIndex = 0;
	return true;
}